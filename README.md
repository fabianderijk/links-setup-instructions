# README #

This list links of software / apps / nice sites

### Drupal ###
* [Drupal CLI](https://github.com/mglaman/drupalorg-cli)
* [Disable Drupal 8 caching during development](https://www.drupal.org/node/2598914)
* [Drush](http://docs.drush.org/en/master/install/)
* [Drush launcher](https://github.com/drush-ops/drush-launcher)
* [Drupal launcher](https://drupalconsole.com/docs/en/getting/launcher)

### LAMP ###
* [macOS 10.13 High Sierra Apache Setup: Multiple PHP Versions](https://getgrav.org/blog/macos-sierra-apache-multiple-php-versions)
* [Valet+](https://github.com/weprovide/valet-plus)
* [NVM](https://github.com/nvm-sh/nvm)
	* [NVM Shell integration](https://github.com/nvm-sh/nvm#deeper-shell-integration)

### Mac apps ###
* [Spectacle - window manager](https://www.spectacleapp.com)
* [Flycut - clipboard manager](https://itunes.apple.com/nl/app/flycut-clipboard-manager/id442160987?mt=12)
* [Caffeine - Do not sleep](http://lightheadsw.com/caffeine/)
* [Stack Storage](https://www.transip.nl/cp/stack/download/)
* [LaunchControl - Create, manage and debug launchd(8) services](http://www.soma-zone.com/LaunchControl/)
* [Postman - API development](https://www.getpostman.com)
* [SourceTree - Git client](https://www.sourcetreeapp.com)
* [Slack](https://slack.com)
* [Visual Studio Code](https://code.visualstudio.com)
* [Atom](https://atom.io)

### Help ###
* [Move SSH Keys From One Computer to Another](https://osxdaily.com/2012/07/13/move-ssh-keys-from-one-computer-to-another/)

### Install scripts
* Install mcrypt on > PHP 7.2: `pecl install mcrypt-1.0.1`

### Code samples
Add to .zshrc:

```
# Auto load nvmrc files
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc

# Source aliasses, highlighting, etc
source ~/.aliasses

export XDEBUG_CONFIG="idekey=PHPSTORM"
export HOMEBREW_DEVELOPER=1
export COMPOSER_MEMORY_LIMIT=-1
```